#!/bin/bash

EPUB2="Pierre Kropotkine - La loi et l'autorité_epub2.epub"
EPUB3="Pierre Kropotkine - La loi et l'autorité_epub3.epub"

zip -X -Z store "$EPUB2" "book/mimetype"
zip -rg "$EPUB2" "book/META-INF/" "book/OEBPS/" -x \*.DS_STORE 
zip -X -Z store "$EPUB3" "book3/mimetype"
zip -rg "$EPUB3" "book3/META-INF/" "book/OEBPS/" -x \*.DS_STORE 

pandoc -f markdown -t html -o "description.html" "description.md"
