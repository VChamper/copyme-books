#!/bin/bash

EPUB2="Pierre Kropotkine - La loi et l'autorité_epub2.epub"
EPUB3="Pierre Kropotkine - La loi et l'autorité_epub3.epub"

zip -X -Z store "$EPUB2" "book/mimetype"
zip -r "$EPUB2" "book/META-INF/" "book/OEBPS/"
zip -X -Z store "$EPUB3" "book/mimetype"
zip -r "$EPUB3" "book/META-INF/" "book/OEBPS/"

pandoc -f markdown -t html -o "description.html" "description.md"
