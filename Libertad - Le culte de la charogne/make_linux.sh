#!/bin/bash

EPUB2="Libertad - Le culte de la charogne_epub2.epub"
EPUB3="Libertad - Le culte de la charogne_epub3.epub"

zip -X -Z store "$EPUB2" "book/mimetype"
zip -r "$EPUB2" "book/META-INF/" "book/OEBPS/"
zip -X -Z store "$EPUB3" "book3/mimetype"
zip -r "$EPUB3" "book3/META-INF/" "book/OEBPS/"

pandoc -f markdown -t html -o "description.html" "description.md"
