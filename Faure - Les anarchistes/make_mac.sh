#!/bin/bash

EPUB="Sebastien Faure - Les anarchistes, ce qu’ils ne sont, ce qu’ils ne sont pas.epub"

zip -X -Z store "$EPUB" "book/mimetype"
zip -rg "$EPUB" "book/META-INF/" "book/OEBPS/" -x \*.DS_STORE 

pandoc -f markdown -t html -o "description.html" "description.md"
