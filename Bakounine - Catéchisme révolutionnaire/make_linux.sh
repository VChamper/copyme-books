#!/bin/bash

EPUB="Bakounine − Catéchisme révolutionnaire.epub"

zip -X -Z store "$EPUB" "book/mimetype"
zip -r "$EPUB" "book/META-INF/" "book/OEBPS/"

pandoc -f markdown -t html -o "description.html" "description.md"
