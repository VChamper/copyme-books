**« Principes généraux**

*Négation de l’existence d’un dieu réel, extra mondial, personnel, et par conséquent aussi de toute révélation et de toute intervention divine dans les affaires du monde et de l’humanité. Abolition du service et du culte de la divinité.*

*Remplaçant le culte de Dieu par le respect et l’amour de l’humanité, nous affirmons la raison humaine, comme critérium unique de la vérité ; la conscience humaine, comme base de la justice ; la liberté individuelle et collective, comme unique créateur de l’ordre de l’humanité.*

*[…] Il n’est point vrai que la liberté d’un homme soit limitée par celle de tous les autres. L’homme n’est réellement libre qu’autant que sa liberté, librement reconnue et représentée comme par un miroir par la conscience libre de tous les autres, trouve la confirmation de son extension à l’infini dans leur liberté. L’homme n’est vraiment libre que parmi les autres hommes également libres ; et comme il n’est libre qu’à titre d’humain, l’esclavage d’un seul homme sur la terre, étant une offense contre le principe même de l’humanité, est la négation de la liberté de tous. La liberté de chacun n’est donc réalisable que dans l’égalité de tous.*

*[…] Respecter la liberté de son prochain, c’est le devoir ; l’aimer, l’aider, le servir, c’est la vertu. Exclusion absolue de tout principe d’autorité et de raison d’État.*

*[…] L’ordre dans la société doit être la résultante du plus grand développement possible de toutes les libertés locales, collectives et individuelles. L’organisation politique et économique de la vie sociale doit partir, par conséquent, non plus comme aujourd’hui de haut en bas et du centre à la circonférence par principe d’unité et de centralisation forcées, mais de bas en haut et de la circonférence au centre, par principe d’association et de fédération libres. »*
