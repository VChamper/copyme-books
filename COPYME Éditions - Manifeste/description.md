*« La lecture numérique gagne lentement du terrain. Dans les pays anglosaxons, elle occupe une bonne part du marché du livre, en France, une part moindre mais croissante.*

*Cela implique qu’à l’avenir, comme une partie des manuscrits d’avant l’invention de l’imprimerie, ce qui ne sera pas transformé en livre numérique sera oublié, plongé dans la masse. Abandonné.*

*Cela implique pour le moment que seules les livres numériques commerciaux disposeront d’éditions numérique de qualité, de belles typographies, de belles mises en page. Les livres moins consensuels seront transformés, eux aussi, mais plus lentement, et seront de moins bonne qualité.*

*À moins que… »*
