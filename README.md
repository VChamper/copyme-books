# COPYME Éditions books

Books of the [COPYME Éditions](http://tviblindi.legtux.org/copyme/).

## Generate a book

### Make

Move to the book of your choice's folder and execute:

#### Linux

`make_linux`

#### Mac OS

`make_mac`

#### Windows

### What you get

1. A EPUB file
2. A description.html file wich contains an extract/presentation of the book.

## Make a new book

**You MUST have the rights to make this book! COPYME Éditions official books are only under Public Domain or free/open-source licenses.**

1. Read the COPYME Éditions Manifesto
2. Get [COPYME Éditions kit](https://git.framasoft.org/VChamper/copyme-kit/)
3. Use the [example book](https://git.framasoft.org/VChamper/copyme-kit/blob/master/example-book/) folder to make yours
4. Have some days of work!
5. Make a pull request!

