*« Je veux être utile, je veux que nous soyons utiles. Je veux être utile à mon voisin et je veux que mon voisin me soit utile. Je désire que nous œuvrions beaucoup car je suis insatiable de jouissance. Et c’est parce que je veux jouir que je ne suis résigné.*

*Oui, oui, je veux produire, mais je veux jouir ; je veux pétrir la pâte, mais manger du meilleur pain ; faire la vendange, mais boire du meilleur vin ; construire la maison mais habiter de meilleur appartement ; faire les meubles, mais posséder l’utile, voire le beau ; je veux faire faire des théâtres, mais assez vaste pour y loger les miens et moi.*

*Je veux coopérer à produire, mais je veux coopérer à consommer.*

*Que les uns rêvent de produire pour d’autres à qui ils laisseront, ô ironie, le meilleur de leurs efforts, pour moi je veux, groupé librement, produire mais consommer.*

*Résignés, regardez, je crache sur vos idoles, je crache sur Dieu, je crache sur la patrie, je crache sur le Christ, je crache sur les drapeaux, je crache sur le capital et sur le veau d’or, je crache sur les religions : ce sont des hochets, je m’en moque, je m’en ris…*

*Ils ne sont rien que par vous, quittez-les et ils se brisent en miettes. »*
