#!/bin/bash

EPUB="Collectif - Contre le travail et ses apôtres.epub"

zip -X -Z store "$EPUB" "book/mimetype"
zip -r "$EPUB" "book/META-INF/" "book/OEBPS/"

pandoc -f markdown -t html -o "description.html" "description.md"
