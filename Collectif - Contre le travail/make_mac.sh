#!/bin/bash

EPUB="Collectif - Contre le travail et ses apôtres.epub"

zip -X -Z store "$EPUB" "book/mimetype"
zip -rg "$EPUB" "book/META-INF/" "book/OEBPS/" -x \*.DS_STORE 

pandoc -f markdown -t html -o "description.html" "description.md"
