*« Il est désormais évident que la Russie soviétique est, sur le plan politique, un régime de despotisme absolu et, sur le plan économique, la forme la plus grossière du capitalisme d’État. »*
