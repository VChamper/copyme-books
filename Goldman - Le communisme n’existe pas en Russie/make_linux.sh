#!/bin/bash

EPUB="Emma Goldman - Le communisme n’existe pas en russie.epub"

zip -X -Z store "$EPUB" "book/mimetype"
zip -r "$EPUB" "book/META-INF/" "book/OEBPS/"

pandoc -f markdown -t html -o "description.html" "description.md"
