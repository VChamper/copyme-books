#!/bin/bash

EPUB="Déclaration des droits de l'homme et du citoyen de 1789.epub"

zip -X -Z store "$EPUB" "book/mimetype"
zip -rg "$EPUB" "book/META-INF/" "book/OEBPS/" -x \*.DS_STORE 

pandoc -f markdown -t html -o "description.html" "description.md"
