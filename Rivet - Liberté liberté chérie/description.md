*« Les bons citoyens récitent des oraisons, font la queue, bien en ordre, devant les percepteurs. Ils font la queue, aussi, devant les bureaux de vote où s’élaborent les majorités futures. Les bons citoyens applaudissent le ministre Machin lorsqu’il a renversé le ministre Chose, puis applaudissent le ministre Chose lorsqu’à son tour il a renversé le ministre Machin. Il ne leur vient jamais à l’idée de se débarrasser de Chose et de Machin.*

*Le bon citoyen vote, paie, applaudit.*

*Il fait comme les autres :*

*— Bée ! Bée ! Bée !… »*
