*« C’est toi le criminel, ô Peuple, puisque c’est toi le Souverain. Tu es, il est vrai, le criminel inconscient et naïf. Tu votes et tu ne vois pas que tu es ta propre victime.*

*Pourtant n’as-tu pas encore assez expérimenté que les députés, qui promettent de te défendre, comme tous les gouvernements du monde présent et passé, sont des menteurs et des impuissants ?*

*Tu le sais et tu t’en plains ! Tu le sais et tu les nommes ! Les gouvernants quels qu’ils soient, ont travaillé, travaillent et travailleront pour leurs intérêts, pour ceux de leurs castes et de leurs coteries.*

*Où en a-t-il été et comment pourrait-il en être autrement ? Les gouvernés sont des subalternes et des exploités : en connais-tu qui ne le soient pas ?*

*Tant que tu n’as pas compris que c’est à toi seul qu’il appartient de produire et de vivre à ta guise, tant que tu supporteras, - par crainte,- et que tu fabriqueras toi-même, - par croyance à l’autorité nécessaire,- des chefs et des directeurs, sache-le bien aussi, tes délégués et tes maîtres vivront de ton labeur et de ta niaiserie. »*
