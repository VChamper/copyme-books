#!/bin/bash

EPUB="Irène Zeilinger - Non c’est non.epub"

zip -X -Z store "$EPUB" "book/mimetype"
zip -rg "$EPUB" "book/META-INF/" "book/OEBPS/" -x \*.DS_STORE 

pandoc -f markdown -t html -o "description.html" "description.md"
