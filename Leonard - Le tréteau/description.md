*Ah ! ma foi, pour le coup, celui-ci me dépasse !*

*Il ne nous flatte pas, il nous dit même en face*

*Que nous n'avons ni cœur, ni sens, ni dignité.*

*Il ne veut être rien, pas même député.*

*Il crie à ceux d'en bas de relever la tête.*

*Voudrait-il nous placer au-dessus de la bête,*

*Et que, libre, chacun pût faire son chemin*

*Sans être retenu par le col ou la main ?*
